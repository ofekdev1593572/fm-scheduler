import state from './admins.state'
import mutations from './admins.mutations'
import  actions from './admins.actions'

export default {
  namespaced: true,
  mutations,
  actions,
  state
}
